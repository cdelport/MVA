#include <iostream>
#include <TFile.h>

#include <TSystem.h>

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <time.h>         // time_t, time, ctime

#include "TMVARootFoldTester.h"

#include "colormod.h"

TMVARootFoldTester::TMVARootFoldTester(TString fileIn, TString comment)
{
  Color::Modifier def(Color::FG_DEFAULT);
  Color::Modifier red(Color::FG_RED);
  Color::Modifier blue(Color::FG_BLUE);
  Color::Modifier green(Color::FG_GREEN);

  m_comment = comment;

  m_rebinMethod = 6;
  m_rebinMaxUncertainty = 0.05;

  TFile *file = new TFile(fileIn, "READ");

  if(file == NULL)
    {
      std::cout  << "Could not find TMVA.root file in " << fileIn << " : call a throw exception ..." << std::endl;
      throw;
    }
  
  m_mvaTree = new TTree();
  m_mvaTree = (TTree*)file->GetObjectChecked("TestTree","TTree");
  
  int nCategories = this->CountBDTCategories();

  for (int c=0; c<nCategories; c++)
    {
      this->SetComment(""); // dummy
      this->FillMVAHist(c+1);
      this->RebinHists();
      this->ComputeSensitivity();
      Double_t sensitivity = this->Sensitivity();
      Double_t uncertainty = this->Uncertainty();
      m_sensitivities.push_back(sensitivity);
      m_uncertainties.push_back(uncertainty);
      std::cout << green << "Sensitivity in category " << c << " = " << sensitivity << " +- " << uncertainty << def << std::endl;
    }

  file->Close();
  delete file;
  file = NULL;
}

TMVARootFoldTester::~TMVARootFoldTester()
{
}

void TMVARootFoldTester::FillMVAHist(Double_t category)
{
  m_mvaHistSig = new TH1F( TString("MVAHistSig") + this->Comment(), "MVA Signal Distribution", 1000, -1., 1.);
  m_mvaHistBkg = new TH1F( TString("MVAHistBkg") + this->Comment(), "MVA Background Distribution", 1000, -1., 1.);


  static Float_t nJ, mva, weight, cat;
  static char eventType;
  TString bdtBranch = this->FindBDTBranch();
  std::cout << "BDT branch " << bdtBranch << std::endl;
  
  m_mvaTree->SetBranchStatus("nJ", 1);
  m_mvaTree->SetBranchStatus(bdtBranch, 1);
  m_mvaTree->SetBranchStatus("weight", 1);
  m_mvaTree->SetBranchStatus("className", 1);
  m_mvaTree->SetBranchStatus(TString::Format("%s_cat%i", bdtBranch.Data(), (int)category), 1);
  
  m_mvaTree->SetBranchAddress("nJ", &nJ);
  m_mvaTree->SetBranchAddress(bdtBranch, &mva);
  m_mvaTree->SetBranchAddress("weight", &weight);
  m_mvaTree->SetBranchAddress("className", &eventType);
  m_mvaTree->SetBranchAddress(TString::Format("%s_cat%i", bdtBranch.Data(), (int)category), &cat);
  
  for(Int_t i=0; i<m_mvaTree->GetEntries(); i++)
    {
      m_mvaTree->GetEntry(i);

      if(cat != 1) continue;
      
      if(eventType == std::string("S"))
	m_mvaHistSig->Fill(mva, weight);
      else if(eventType == std::string("B"))
	m_mvaHistBkg->Fill(mva, weight);
    }
}


void TMVARootFoldTester::ComputeSensitivity()
{
  if(m_mvaHistBkg->GetNbinsX() != m_mvaHistSig->GetNbinsX())
    {
      std::cout << "Number of bins Sig-Bkg does not match" << std::endl;
      return;
    }

  Float_t sensitivity = 0.;
  Float_t uncertainty = 0.;
  
  Int_t nBins = m_mvaHistSig->GetNbinsX();
  for(Int_t i=0; i<nBins; i++)
    {
      double iS = m_mvaHistSig->GetBinContent(i+1);
      double iB = m_mvaHistBkg->GetBinContent(i+1);
      
      double idS = m_mvaHistSig->GetBinError(i+1);
      double idB = m_mvaHistBkg->GetBinError(i+1);

      if(iS<=0 or iB<=0)
	continue;
      
      double iLSB = log(1+iS/iB);
      double s = 2*((iS+iB)*iLSB-iS);
      double u = iLSB * iLSB * idS * idS + (iLSB - iS/iB) * (iLSB - iS/iB) * idB * idB;
      
      sensitivity += s;
      uncertainty += u;
    }

  m_sensitivity = sqrt(sensitivity);
  if (m_sensitivity > 0.)
    uncertainty = sqrt(uncertainty)/m_sensitivity;
  m_uncertainty = uncertainty;

 delete m_mvaHistSig;
 delete m_mvaHistBkg;
 m_mvaHistSig = NULL;
 m_mvaHistBkg = NULL;
}


void TMVARootFoldTester::RebinHists()
{
  HistoTransform histoTransform;
  
  std::vector<int> rebinnedBins;
  rebinnedBins = histoTransform.getRebinBins(m_mvaHistBkg, m_mvaHistSig, m_rebinMethod, m_rebinMaxUncertainty);

  TFile *outFile = new TFile("rebinHists_" + this->Comment() + ".root","RECREATE");
  outFile->cd();
  m_mvaHistSig->Write("sig");
  m_mvaHistBkg->Write("bkg");
  
  histoTransform.rebinHisto(m_mvaHistSig, &rebinnedBins, true, false);
  histoTransform.rebinHisto(m_mvaHistBkg, &rebinnedBins, true, false);

  m_mvaHistSig->Write("sig_rebin");
  m_mvaHistBkg->Write("bkg_rebin");

  if (this->Comment().Contains("2j"))
    m_rebinnedBins_2j = rebinnedBins;
  if (this->Comment().Contains("3j"))
    m_rebinnedBins_3j = rebinnedBins;

  outFile->Close();
  
  return;
}


std::vector<TString> TMVARootFoldTester::arrayList2Vec(TObjArray *arrayList)
{
  std::vector<TString> vect;
  for (Int_t i = 0; i < arrayList->GetEntries(); i++)
    vect.push_back( ((TObjString *)(arrayList->At(i)))->String() );

  return vect;
}


TString TMVARootFoldTester::FindBDTBranch()
{
  std::vector<TString> branchList = arrayList2Vec(m_mvaTree->GetListOfBranches());
  for (auto b:branchList)
    if (b.Contains("BDTCategories") and !(b.Contains("_cat")))
      return b;
  return TString("");
}

int TMVARootFoldTester::CountBDTCategories()
{
  int nCategories = 0;
  std::vector<TString> branchList = arrayList2Vec(m_mvaTree->GetListOfBranches());
  for (auto b:branchList)
    {
      if (b.Contains("BDTCategories_") and b.Contains("_cat"))
	nCategories++;
    }

  std::cout << "Number of categories in TestTree : " << nCategories << std::endl;
  
  return nCategories;
}
