#include <iostream>
#include <TFile.h>

#include <TSystem.h>

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <time.h>         // time_t, time, ctime

#include "TMVARootTester.h"

#include "colormod.h"

TMVARootTester::TMVARootTester(TString fileIn, TString comment)
{
  Color::Modifier def(Color::FG_DEFAULT);
  Color::Modifier red(Color::FG_RED);
  Color::Modifier blue(Color::FG_BLUE);
  Color::Modifier green(Color::FG_GREEN);

  m_comment = comment;

  m_rebinMethod = 6;
  m_rebinMaxUncertainty = 0.05;

  TFile *file = new TFile(fileIn, "READ");

  if(file == NULL)
    {
      std::cout << "Could not find TMVA.root file in " << fileIn << " : call a throw exception ..." << std::endl;
      throw;
    }
  
  m_mvaTree = new TTree();
  m_mvaTree = (TTree*)file->GetObjectChecked("TestTree","TTree");

  this->SetComment( this->Comment() + "2j" );
  this->FillMVAHist(2);
  this->RebinHists();
  this->ComputeSensitivity();
  Double_t sensitivity2j = this->Sensitivity();
  Double_t uncertainty2j = this->Uncertainty();
  std::cout << "Sensitivity 2j = " << sensitivity2j << " +- " << uncertainty2j << std::endl;

  // handle some substitutions if 2j/3j in separate files
  if (fileIn.Contains("2j"))
    {
      file->Close();

      fileIn.ReplaceAll("2j", "3j");
      file = new TFile(fileIn, "READ");
      m_mvaTree = new TTree();
      m_mvaTree = (TTree*)file->GetObjectChecked("TestTree","TTree");
    }
  
  this->SetComment( this->Comment().ReplaceAll("2j", "3j") );
  this->FillMVAHist(3);
  this->RebinHists();
  this->ComputeSensitivity();
  Double_t sensitivity3j = this->Sensitivity();
  Double_t uncertainty3j = this->Uncertainty();
  std::cout << "Sensitivity 3j = " << sensitivity3j << " +- " << uncertainty3j << std::endl;

  m_sensitivity = sqrt( sensitivity2j*sensitivity2j + sensitivity3j*sensitivity3j );
  m_uncertainty = sqrt(sensitivity2j*sensitivity2j*uncertainty2j*uncertainty2j + sensitivity3j*sensitivity3j*uncertainty3j*uncertainty3j) / m_sensitivity;
  std::cout << green  << "Combined sensitivity = " << m_sensitivity << def << std::endl;
  
  file->Close();
  delete file;
  file = NULL;
}

TMVARootTester::~TMVARootTester()
{
}

void TMVARootTester::FillMVAHist(Double_t nJCut)
{
  m_mvaHistSig = new TH1F( TString("MVAHistSig") + this->Comment(), "MVA Signal Distribution", 1000, -1., 1.);
  m_mvaHistBkg = new TH1F( TString("MVAHistBkg") + this->Comment(), "MVA Background Distribution", 1000, -1., 1.);


  static Float_t nJ, mva, weight;
  static char eventType;
  TString bdtBranch = this->FindBDTBranch();
  std::cout << "BDT branch " << bdtBranch << std::endl;
  
  m_mvaTree->SetBranchStatus("nJ", 1);
  m_mvaTree->SetBranchStatus(bdtBranch, 1);
  m_mvaTree->SetBranchStatus("weight", 1);
  m_mvaTree->SetBranchStatus("className", 1);
  
  m_mvaTree->SetBranchAddress("nJ", &nJ);
  m_mvaTree->SetBranchAddress(bdtBranch, &mva);
  m_mvaTree->SetBranchAddress("weight", &weight);
  m_mvaTree->SetBranchAddress("className", &eventType);
  
  for(Int_t i=0; i<m_mvaTree->GetEntries(); i++)
    {
      m_mvaTree->GetEntry(i);

      if(nJ != nJCut) continue;
      
      if(eventType == std::string("S"))
	m_mvaHistSig->Fill(mva, weight);
      else if(eventType == std::string("B"))
	m_mvaHistBkg->Fill(mva, weight);
    }
}


void TMVARootTester::ComputeSensitivity()
{
  if(m_mvaHistBkg->GetNbinsX() != m_mvaHistSig->GetNbinsX())
    {
      std::cout << "Number of bins Sig-Bkg does not match" << std::endl;
      return;
    }

  Float_t sensitivity = 0.;
  Float_t uncertainty = 0.;
  
  Int_t nBins = m_mvaHistSig->GetNbinsX();
  for(Int_t i=0; i<nBins; i++)
    {
      double iS = m_mvaHistSig->GetBinContent(i+1);
      double iB = m_mvaHistBkg->GetBinContent(i+1);
      
      double idS = m_mvaHistSig->GetBinError(i+1);
      double idB = m_mvaHistBkg->GetBinError(i+1);

      if(iS<=0 or iB<=0)
	continue;
      
      double iLSB = log(1+iS/iB);
      double s = 2*((iS+iB)*iLSB-iS);
      double u = iLSB * iLSB * idS * idS + (iLSB - iS/iB) * (iLSB - iS/iB) * idB * idB;
      
      sensitivity += s;
      uncertainty += u;
    }

  m_sensitivity = sqrt(sensitivity);
  if (m_sensitivity > 0.)
    uncertainty = sqrt(uncertainty)/m_sensitivity;
  m_uncertainty = uncertainty;

 delete m_mvaHistSig;
 delete m_mvaHistBkg;
 m_mvaHistSig = NULL;
 m_mvaHistBkg = NULL;
}


void TMVARootTester::RebinHists()
{
  HistoTransform histoTransform;
  
  std::vector<int> rebinnedBins;
  rebinnedBins = histoTransform.getRebinBins(m_mvaHistBkg, m_mvaHistSig, m_rebinMethod, m_rebinMaxUncertainty);

  TFile *outFile = new TFile("rebinHists_" + this->Comment() + ".root","RECREATE");
  outFile->cd();
  m_mvaHistSig->Write("sig");
  m_mvaHistBkg->Write("bkg");
  
  histoTransform.rebinHisto(m_mvaHistSig, &rebinnedBins, true, false);
  histoTransform.rebinHisto(m_mvaHistBkg, &rebinnedBins, true, false);

  m_mvaHistSig->Write("sig_rebin");
  m_mvaHistBkg->Write("bkg_rebin");

  if (this->Comment().Contains("2j"))
    m_rebinnedBins_2j = rebinnedBins;
  if (this->Comment().Contains("3j"))
    m_rebinnedBins_3j = rebinnedBins;

  outFile->Close();
  
  return;
}


std::vector<TString> TMVARootTester::arrayList2Vec(TObjArray *arrayList)
{
  std::vector<TString> vect;
  for (Int_t i = 0; i < arrayList->GetEntries(); i++)
    vect.push_back( ((TObjString *)(arrayList->At(i)))->String() );

  return vect;
}


TString TMVARootTester::FindBDTBranch()
{
  std::vector<TString> branchList = arrayList2Vec(m_mvaTree->GetListOfBranches());
  for (auto b:branchList)
    if (b.Contains("BDTCategories") and !(b.Contains("_cat")))
      return b;
  return TString("");
}
