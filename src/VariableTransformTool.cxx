#include "VariableTransformTool.h"

#include <vector>

#include <TTree.h>

VariableTransformTool::VariableTransformTool(TString varName, int nBins, double min, double max, double eff)
{
  m_varName = varName;
  m_min     = min;
  m_max     = max;
  m_cut2j   = -1.;
  m_cut3j   = -1.;
  m_eff     = eff;
  m_signal     = new TH2F("transfo_"+varName+"_signal", varName, nBins, m_min, m_max, 2, 2., 4.); // y-axis stands for nJ
  m_background = new TH2F("transfo_"+varName+"_background", varName, nBins, m_min, m_max, 2, 2., 4.);
}

VariableTransformTool::~VariableTransformTool()
{
}

void VariableTransformTool::AddSignalEvent(double val, double nJ, double weight)
{
  m_signal->Fill(val, nJ, weight);
}

void VariableTransformTool::AddBackgroundEvent(double val, double nJ, double weight)
{
  m_background->Fill(val, nJ, weight);
}

double VariableTransformTool::TrainCut1D(int nJ)
{
  TH1F *signal1D  = (TH1F*)m_signal->ProjectionX("signal1D", nJ-1, nJ-1);
  TH1F *signalCum = (TH1F*)signal1D->GetCumulative();
  double integral = signal1D->Integral();
  signalCum->Scale(1./integral);
  
  int cutIndex = signalCum->FindFirstBinAbove(m_eff, 1);
  double cut = signal1D->GetXaxis()->GetBinUpEdge(cutIndex);

  delete signal1D;
  signal1D = NULL;
  
  delete signalCum;
  signalCum = NULL;
  
  return cut;
}

void VariableTransformTool::TrainCut()
{
  std::cout << "--- VariableTransformTool : Training cuts for variable " << this->GetName() << std::endl;
  
  double cut2j = this->TrainCut1D(2);
  std::cout << "--- VariableTransformTool - 2 jets category --> Computed cut : " << this->GetName() << " < " << cut2j << std::endl;
  this->SetCut2j(cut2j);
  
  double cut3j = this->TrainCut1D(3);
  std::cout << "--- VariableTransformTool - 3 jets category --> Computed cut : " << this->GetName() << " < " << cut3j << std::endl;

  this->SetCut3j(cut3j);

}


double VariableTransformTool::GetNewValue(double val, double nJ)
{
  double valNew = -99.;
  valNew = this->Pass(val, nJ) ? val : this->GetCut(nJ);
  return valNew;
}


void VariableTransformTool::Train()
{
  this->TrainCut();
}
