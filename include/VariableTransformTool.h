#ifndef VARIABLETRANSFORMTOOL_H
#define VARIABLETRANSFORMTOOL_H

#include <iostream>

#include <TAxis.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>

class VariableTransformTool {

public :
  
  VariableTransformTool(TString varName, int nBins, double min, double max, double eff);
  ~VariableTransformTool();

  TString GetName() { return m_varName; }
  
  void AddSignalEvent(double val, double nJ, double weight);
  void AddBackgroundEvent(double val, double nJ, double weight);
 
  void SetCut2j(double cut) { m_cut2j = cut; }
  void SetCut3j(double cut) { m_cut3j = cut; }
  double GetCut(double nJ) { return (nJ < 2.5) ? m_cut2j : m_cut3j; }
  void SetEff(double eff) { m_eff = eff; }

  void Train();
  
  void TrainCut();
  double TrainCut1D(int nJ);
  bool Pass(double val, double nJ) { return (nJ < 2.5) ? (val < m_cut2j) : (val < m_cut3j); }

  double GetNewValue(double val, double nJ);
  
private :

  TString m_varName;
  double m_min;
  double m_max;
  double m_cut2j;
  double m_cut3j;
  double m_eff;
  TH2F *m_signal;
  TH2F *m_background;

  TAxis *m_purityAxis;

};

#endif
