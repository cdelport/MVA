#ifndef TMVAROOTTESTER_H
#define TMVAROOTTESTER_H

#include <vector>

#include <TTree.h>
#include <TString.h>
#include <TObjArray.h>
#include <TH1F.h>

#include "HistoTransform.h"

class TMVARootTester {

public:
  TMVARootTester(TString fileIn, TString comment = "");
  ~TMVARootTester();
  
  void FillMVAHist(Double_t nJCut);
  void ComputeSensitivity();

  void SetComment(TString comment) {m_comment = comment;};
  
  Float_t Sensitivity() { return m_sensitivity; };
  Float_t Uncertainty() { return m_uncertainty; };
  TString Comment() { return m_comment; };
  TH1F MVAHist();

  void SetRebinMethod(Int_t method) { m_rebinMethod = method; };
  void SetRebinMaxUncertainty(Double_t rebinMaxUncertainty) { m_rebinMaxUncertainty = rebinMaxUncertainty; };

  void RebinHists();

  std::vector<TString> arrayList2Vec(TObjArray *arrayList);
  TString FindBDTBranch();

  std::vector<int> getRebinnedAxis(int nJ) { return (nJ ==2) ? m_rebinnedBins_2j : m_rebinnedBins_3j; };


private:
  TH1F*   m_mvaHistSig;
  TH1F*   m_mvaHistBkg;
  TTree*  m_mvaTree;
  TString m_comment;
  Float_t m_sensitivity;
  Float_t m_uncertainty;

  Int_t    m_rebinMethod;
  Double_t m_rebinMaxUncertainty;

  // rebinned axis
  std::vector<int> m_rebinnedBins_2j;
  std::vector<int> m_rebinnedBins_3j;
};

#endif
