# General
CXX           = g++
CXXFLAGS      = -O2 -Wall -fPIC -g -ansi -std=c++11
LDFLAGS       = -O -L. -lTMVA -lRooFitCore
INCLUDE       = -I. -Iinclude

# ROOT config
ROOTCONFIG   := $(ROOTSYS)/bin/root-config
ROOTCFLAGS   := $(shell $(ROOTCONFIG) --cflags)
ROOTLDFLAGS  := $(shell $(ROOTCONFIG) --ldflags) $(shell $(ROOTCONFIG) --libs)

CXXFLAGS     += $(ROOTCFLAGS)
LDFLAGS      += $(ROOTLDFLAGS)

# local source code
SRCS =  $(wildcard src/*.cxx)
OBJS = $(SRCS:src/%.cxx=lib/%.o)

# dependencies
DEPDIR = .deps
DEPS = $(OBJS:lib/%.o=$(DEPDIR)/%.d)

all :   TMVAClassification variablesOptimization significance significanceFold
	@echo "All OK"

# pull in dependency info for .o files
-include $(DEPS)

lib/%.o : src/%.cxx
	@mkdir -p lib
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@
	@mkdir -p $(DEPDIR)
	@echo lib/`$(CXX) -MM $(CXXFLAGS) $(INCLUDE) $<` | sed 's, \\ , ,g' > $(@:lib/%.o=$(DEPDIR)/%.d)
	@cp -f $(@:lib/%.o=$(DEPDIR)/%.d) $(@:lib/%.o=$(DEPDIR)/%.d.tmp)
	@sed -e 's/.*://' -e 's/\\$$//' < $(@:lib/%.o=$(DEPDIR)/%.d.tmp) | fmt -1 | \
	sed -e 's/^ *//' -e 's/$$/:/' >> $(@:lib/%.o=$(DEPDIR)/%.d)
	@rm -f $(@:lib/%.o=$(DEPDIR)/%.d.tmp)

$(DEPDIR)/%.d:
	@rm -f $(@:$(DEPDIR)/%.d=lib/%.o)

TMVAClassification : $(DEPS) $(OBJS) TMVAClassification.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS) $(OBJS) TMVAClassification.cxx -o TMVAClassification
variablesOptimization : $(DEPS) $(OBJS) variablesOptimization.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS) $(OBJS) variablesOptimization.cxx -o variablesOptimization
significance : $(DEPS) $(OBJS) significance.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS) $(OBJS) significance.cxx -o significance
significanceFold : $(DEPS) $(OBJS) significanceFold.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS) $(OBJS) significanceFold.cxx -o significanceFold

clean : clean~
	rm -rf lib
	rm -rf $(DEPDIR)
	rm -rf bin

clean~ :
	rm -rf *~
	rm -rf src/*~
	rm -rf include/*~
