#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <map>
#include <string>
#include <vector>

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TObjString.h"


#include <TSystem.h>
#include <TROOT.h>
#include <TStopwatch.h>

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/TMVAGui.h"
#include "TMVA/MethodCategory.h"

#include "VariableTransformTool.h"

using namespace std;

bool useCommentInName       = false;
bool useScaleFactorsFromFit = false;

enum SampleType { Signal, Background };
struct MVAInput { SampleType type;
                  TString    name;
                  TChain     *tree;
};

enum SignalSample { VH, Diboson, NotDefined };
enum SignalSample signalSample = NotDefined;

enum VariableType { Spectator, Training };
struct variable { VariableType type;
                  TString      name;
                  int          id;
};
map<TString, int> mapVarID;

int               nFold     = 2;
int               threshold = 10;

void addEventsToFactory(TMVA::Factory *factory, vector<variable> variables, MVAInput& input, map<TString, VariableTransformTool *> trafos);
double getFitScaleFactor(int nJ, std::string sample);

vector<TString> arrayList2Vec(TObjArray *arrayList);

vector<TString> GetTokens(TString list, TString sep);


int TMVAClassification(TString dataset, TString sigChainList, TString bkgChainList, TString varChainList, TString var3jChainList, TString comment, TString options)
{
   TStopwatch watch;

   watch.Start();

   // Retrieve parameters in a better format
   vector<TString> sigList   = GetTokens(sigChainList, ":");
   vector<TString> bkgList   = GetTokens(bkgChainList, ":");
   vector<TString> varList   = GetTokens(varChainList, ":");
   vector<TString> var3jList = GetTokens(var3jChainList, ":");

   TMVA::Tools::Instance();

   std::cout << "\n==> Start TMVAClassification" << std::endl;

   // --------------------------------------------------------------------------------------------------
   // Create a ROOT output file where TMVA will store ntuples, histograms, etc.
   TString outfileName("TMVA.root");

   TFile   *outputFile = TFile::Open(outfileName, "RECREATE");

   // Create the factory object.
   TMVA::Factory *factory = new TMVA::Factory("TMVAClassification", outputFile, "!V:!Silent:Color:AnalysisType=Classification");  // DrawProgressBar

   // the order of variables is: training + targets + spectators
   int              pos = 0;
   vector<variable> variables;
   for (TString& var:varList)
   {
      variables.push_back(variable { Training, var, pos++ });
   }
   for (TString& var:var3jList)
   {
      variables.push_back(variable { Training, var, pos++ });
   }

   variables.push_back(variable { Spectator, "nJ", pos++ });
   variables.push_back(variable { Spectator, "EventNumberMod2", pos++ });
   variables.push_back(variable { Spectator, "ChannelNumber", pos++ });

   for (variable& var:variables)
   {
      cout << "Adding variable " << var.name << endl;
      mapVarID.emplace(var.name, var.id);
      if (var.type == Spectator)
      {
         factory->AddSpectator(var.name); // spectator variables type is float and cannot be changed
      }
      else if (var.type == Training)
      {
         factory->AddVariable(var.name.Data(), 'F');
      }
   }

   // -------------------------------------------
   // Set all input files from their sample name
   // change to a repertory of background samples
   // -------------------------------------------
   vector<MVAInput> inputs;
   for (auto& sig:sigList)
   {
      inputs.push_back(MVAInput { SampleType { Signal }, sig, NULL });

      if ((sig.Contains("ZH") or sig.Contains("WH"))and(signalSample == SignalSample::NotDefined))
      {
         signalSample = SignalSample::VH;
      }
      if ((sig.Contains("ZZ") or sig.Contains("WZ"))and(signalSample == SignalSample::NotDefined))
      {
         signalSample = SignalSample::Diboson;
      }
   }
   for (auto& bkg:bkgList)
   {
      inputs.push_back(MVAInput { SampleType { Background }, bkg, NULL });
   }

   for (MVAInput& input:inputs)
   {
      input.tree = new TChain("Nominal", "chain_" + input.name);
      input.tree->Add("/sps/atlas/d/delporte/MVA/data-MVATree_" + dataset + "/" + input.name.ReplaceAll(" ", "") + "*");
      input.tree->SetBranchStatus("*", 0);
      cout << "Sample " << setw(15) << input.name << " has " << input.tree->GetEntries() << " entries." << endl;
      if (input.tree->GetEntries() < threshold)
      {
         cout << "--- Careful : sample " << input.name << " has very low number of entries and will be ignored in training." << endl;
      }
   }


   // --------------------------------------------
   // handle the transformation of input variables
   // --------------------------------------------
   map<TString, VariableTransformTool *> trafos;

   if (mapVarID.find("MET") != mapVarID.end())
   {
      trafos.emplace("MET", new VariableTransformTool("MET", 750, 0, 750, 0.99));
   }
   if (mapVarID.find("HT") != mapVarID.end())
   {
      trafos.emplace("HT", new VariableTransformTool("HT", 1500, 0, 1500, 0.99));
   }

   if (mapVarID.find("mBB") != mapVarID.end())
   {
      trafos.emplace("mBB", new VariableTransformTool("mBB", 300, 0, 300, 0.99));
   }
   if (mapVarID.find("dRBB") != mapVarID.end())
   {
      trafos.emplace("dRBB", new VariableTransformTool("dRBB", 600, 0, 6, 0.99));
   }
   if (mapVarID.find("dEtaBB") != mapVarID.end())
   {
      trafos.emplace("dEtaBB", new VariableTransformTool("dEtaBB", 600, 0, 6, 0.99));
   }
   if (mapVarID.find("pTB1") != mapVarID.end())
   {
      trafos.emplace("pTB1", new VariableTransformTool("pTB1", 750, 0, 750, 0.99));
   }
   if (mapVarID.find("pTB2") != mapVarID.end())
   {
      trafos.emplace("pTB2", new VariableTransformTool("pTB2", 500, 0, 500, 0.99));
   }
   if (mapVarID.find("pTJ3") != mapVarID.end())
   {
      trafos.emplace("pTJ3", new VariableTransformTool("pTJ3", 500, 0, 500, 0.99));
   }
   if (mapVarID.find("mBBJ") != mapVarID.end())
   {
      trafos.emplace("mBBJ", new VariableTransformTool("mBBJ", 2000, 0, 2000, 0.99));
   }

   if (mapVarID.find("mBBCorr") != mapVarID.end())
   {
      trafos.emplace("mBBCorr", new VariableTransformTool("mBBCorr", 300, 0, 300, 0.99));
   }
   if (mapVarID.find("dRBBCorr") != mapVarID.end())
   {
      trafos.emplace("dRBBCorr", new VariableTransformTool("dRBBCorr", 600, 0, 6, 0.99));
   }
   if (mapVarID.find("dEtaBBCorr") != mapVarID.end())
   {
      trafos.emplace("dEtaBBCorr", new VariableTransformTool("dEtaBBCorr", 600, 0, 6, 0.99));
   }
   if (mapVarID.find("pTB1Corr") != mapVarID.end())
   {
      trafos.emplace("pTB1Corr", new VariableTransformTool("pTB1Corr", 750, 0, 750, 0.99));
   }
   if (mapVarID.find("pTB2Corr") != mapVarID.end())
   {
      trafos.emplace("pTB2Corr", new VariableTransformTool("pTB2Corr", 500, 0, 500, 0.99));
   }
   if (mapVarID.find("pTJ3Corr") != mapVarID.end())
   {
      trafos.emplace("pTJ3Corr", new VariableTransformTool("pTJ3Corr", 500, 0, 500, 0.99));
   }
   if (mapVarID.find("mBBJCorr") != mapVarID.end())
   {
      trafos.emplace("mBBJCorr", new VariableTransformTool("mBBJCorr", 2000, 0, 2000, 0.99));
   }

   for (map<TString, VariableTransformTool *>::iterator trafo = trafos.begin(); trafo != trafos.end(); ++trafo)
   {
      for (MVAInput& input:inputs)
      {
         if (input.type != Signal)
         {
            continue;
         }
         if (input.tree->GetEntries() < threshold)
         {
            continue;                                        // coarse check to avoid inexisting branches
         }
         input.tree->SetBranchStatus("*", 0);

         static Double_t nJ;
         input.tree->SetBranchStatus("nJ", 1);
         input.tree->SetBranchAddress("nJ", &nJ);

         static Double_t EventWeight;
         input.tree->SetBranchStatus("EventWeight", 1);
         input.tree->SetBranchAddress("EventWeight", &EventWeight);

         static Double_t value;
         input.tree->SetBranchStatus(trafo->first.Data(), 1);
         input.tree->SetBranchAddress(trafo->first.Data(), &value);

         for (Long64_t entry = 0; entry < input.tree->GetEntries(); entry++)
         {
            input.tree->GetEntry(entry);
            trafo->second->AddSignalEvent(value, nJ, EventWeight);
         }
      }
      trafo->second->Train();
   }

   // -------------------------------------------
   // add events to the training and test samples
   // -------------------------------------------
   for (MVAInput& input:inputs)
   {
      if (input.tree->GetEntries() < threshold)
      {
         continue;                                       // coarse check to avoid inexisting branches
      }
      input.tree->SetBranchStatus("*", 0);
      addEventsToFactory(factory, variables, input, trafos);
   }

   // --------------------------------------------------------------------------------------------------

   TString trainingOptions = "!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:NEventsMin=100:PruneMethod=NoPruning";
   if (options.Length() > 0)
   {
      trainingOptions += ":VarTransform=";
      trainingOptions += options;
   }

   // --------------------------------------------------------------------------------------------------
   TMVA::MethodCategory *mJetCategories   = 0;
   TMVA::MethodBase     *bdtJetCategories = 0;

   // MethodCategory implementation
   factory->PrepareTrainingAndTestTree("", "NormMode=EqualNumEvents:!V");  // EqualNumEvents
   bdtJetCategories = factory->BookMethod(TMVA::Types::kCategory, TString("BDTCategories") + (useCommentInName ? "_" + comment : ""), "");
   mJetCategories   = dynamic_cast<TMVA::MethodCategory *>(bdtJetCategories);

   for (int j = 2; j < 4; j++) // 4 standing for 4p jets
   {
      for (int fold = 0; fold < nFold; fold++)
      {
         TCut categoryCut;
         if (j < 4)
         {
            categoryCut += "nJ>" + TString::Format("%.1f", j - 0.5);
            categoryCut += "nJ<" + TString::Format("%.1f", j + 0.5);
         }
         else
         {
            categoryCut += "nJ>" + TString::Format("%.1f", j - 0.5); // 4p jets
         }
         categoryCut += "EventNumberMod2%" + TString::Format("%i", nFold) + ">" + TString::Format("%.1f", fold - 0.5);
         categoryCut += "EventNumberMod2%" + TString::Format("%i", nFold) + "<" + TString::Format("%.1f", fold + 0.5);

         TString categoryName = "BDTCat_" + TString::Format("%i", j) + "j_" + TString::Format("%i", fold + 1) + "o" + TString::Format("%i", nFold);
         TString varListTrain = varChainList;
         if (j == 3)
         {
            varListTrain += ":";
            varListTrain += var3jChainList;
         }

         mJetCategories->AddMethod(categoryCut, varListTrain, TMVA::Types::kBDT, categoryName, trainingOptions);
      }
   }

   // --------------------------------------------------------------------------------------------------
   // Train MVAs using the set of training events
   factory->TrainAllMethods();

   // ---- Evaluate all MVAs using the set of test events
   factory->TestAllMethods();

   // ----- Evaluate and compare performance of all configured MVAs
   factory->EvaluateAllMethods();

   // --------------------------------------------------------------
   // Save the output
   outputFile->Close();

   std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
   std::cout << "==> TMVAClassification is done!" << std::endl;

   delete factory;
   factory = NULL;
   delete outputFile;
   outputFile       = NULL;
   mJetCategories   = NULL;
   bdtJetCategories = NULL;

   for (map<TString, VariableTransformTool *>::iterator trafo = trafos.begin(); trafo != trafos.end(); trafo++)
   {
      delete trafo->second;
      trafo->second = NULL;
   }

   for (auto& input:inputs)
   {
      delete input.tree;
      input.tree = NULL;
   }
   inputs.clear();
   variables.clear();
   mapVarID.clear();
   trafos.clear();

   std::cout << "\n==> Running time - ";
   watch.Stop();
   watch.Print();
   std::cout << std::endl;

   return(0);
} /* TMVAClassification */


// --------------------------------------

void addEventsToFactory(TMVA::Factory *factory, vector<variable> variables, MVAInput& input, map<TString, VariableTransformTool *> trafos)
{
   std::cout << "Adding input tree " << input.name << " with " << input.tree->GetEntries() << " entries." << std::endl;

   int              nVariables = variables.size();

   vector<Double_t> event(nVariables);
   input.tree->SetBranchStatus("*", 0);
   for (variable var:variables)
   {
      input.tree->SetBranchStatus(var.name.Data(), 1);
      Int_t status = input.tree->SetBranchAddress(var.name.Data(), &(event[var.id]));
      if (status < 0)
      {
         cout << "Branch " << var.name << " of input " << input.name << " was not set." << endl;
         return;
      }
   }

   int             inJ          = mapVarID.at("nJ");
   int             iEventNumber = mapVarID.at("EventNumberMod2");
   static Double_t ChannelNumber;
   input.tree->SetBranchStatus("ChannelNumber", 1);
   input.tree->SetBranchAddress("ChannelNumber", &ChannelNumber);

   static Double_t EventWeight;
   input.tree->SetBranchStatus("EventWeight", 1);
   input.tree->SetBranchAddress("EventWeight", &EventWeight);

   double weightFitSF = 1.;

   // Diboson MVA : require 2 b-labelled jets as true signal
   /*
    * static Double_t labelB1, labelB2;
    * if (signalSample == SignalSample::Diboson)
    * {
    * input.tree->SetBranchStatus("labelB1", 1);
    * input.tree->SetBranchStatus("labelB2", 1);
    * input.tree->SetBranchAddress("labelB1", &labelB1);
    * input.tree->SetBranchAddress("labelB2", &labelB2);
    * }
    */


   // static TString SampleName;// different from the original, here Zbb (!= from Znunu_B e.g. : Zbb can come from Ztautau_B, Zl can come from Znunu_B, etc.)
   // input.tree->SetBranchStatus("SampleName", 1);
   // input.tree->SetBranchAddress("SampleName", &SampleName);

   for (Long64_t entry = 0; entry < input.tree->GetEntries(); entry++)
   {
      if (false and entry > 1000)
      {
         break;                                               // fast tests
      }

      input.tree->GetEntry(entry);

      if (input.name.Contains("ttbar") or input.name.Contains("ZnunuB_v221") or input.name.Contains("WenuB_v221") or input.name.Contains("WmunuB_v221") or input.name.Contains("WtaunuB_v221"))
      {
        weightFitSF = getFitScaleFactor(event.at(inJ), (std::string)input.name);
      }
      else
      {
        weightFitSF = 1.;
      }

      // EventWeight *= weightFitSF; // default strategy does not require to scale samples weights

      if (event.at(inJ) > 3.5)
      {
         continue;                      // do 4pjet or not
      }
      if (event.at(inJ) < 1.5)
      {
         continue;
      }

      TString inputType = (input.type == Signal) ? "Signal" : ((input.type == Background) ? "Background" : "other");

      // -----------------------------------
      // ChannelNumber selection for Diboson
      // -----------------------------------
      /*
       * ChannelNumber = event.at(mapVarID.at("ChannelNumber"));
       * if ((input.name.Contains("WZ") or input.name.Contains("ZZ")) and (input.type == Signal))
       * {
       * // Diboson MVA : require 2 b-labelled jets as true signal
       * if ((fabs(labelB1) != 5.)or(fabs(labelB2) != 5.))
       * {
       * inputType = "Background";
       * }
       * // Diboson MVA : require 2 jets from the Z boson + Z->vv for the 2nd Z
       * if ((ChannelNumber == 361084)or     // WqqZll
       * (ChannelNumber == 361085) or // WqqZvv
       * (ChannelNumber == 361094) or // WqqZll improved
       * (ChannelNumber == 361095) or // WqqZvv improved
       * (ChannelNumber == 361607) or // WqqZll PwPy8EG
       * (ChannelNumber == 361608) or // WqqZvv PwPy8EG
       * (ChannelNumber == 361096) or // ZqqZll improved
       * (ChannelNumber == 361610))   // ZZqqll ZZ_Pw
       * {
       * inputType = "Background";
       * }
       * }*/

      // -----------------------------------
      // manage transformation of variables
      // -----------------------------------
      for (map<TString, VariableTransformTool *>::iterator trafo = trafos.begin(); trafo != trafos.end(); trafo++)
      {
         int id = mapVarID.at(trafo->first);
         event.at(id) = trafo->second->GetNewValue(event.at(id), event.at(inJ));
      }

      factory->AddTestEvent(inputType, event, EventWeight);
      Double_t EventNumber = event.at(iEventNumber);
      for (int k = 1; k < nFold; k++)
      {
         event.at(iEventNumber) = EventNumber + k;
         factory->AddTrainingEvent(inputType, event, EventWeight);
      }
   } // events
}    /* addEventsToFactory */


double getFitScaleFactor(int nJ, std::string sample)
{
  std::map<std::string, std::map<int, double> > prefit;
  std::map<std::string, std::map<int, double> > postfit;

  prefit["ttbar"][2]   = 436.54;
  prefit["ttbar"][3]   = 3341.71;
  postfit["ttbar"][2]  = 368.86;
  postfit["ttbar"][3]  = 2985.13;

  prefit["ZnunuB_v221"][2]  = 1424.15;
  prefit["ZnunuB_v221"][3]  = 2382.95;
  postfit["ZnunuB_v221"][2] = 2168.93;
  postfit["ZnunuB_v221"][3] = 3507.05;

  prefit["WenuB_v221"][2]     = 305.00;
  prefit["WenuB_v221"][3]     = 783.20;
  postfit["WenuB_v221"][2]    = 486.33;
  postfit["WenuB_v221"][3]    = 1252.10;

  prefit["WmunuB_v221"][2]     = 305.00;
  prefit["WmunuB_v221"][3]     = 783.20;
  postfit["WmunuB_v221"][2]    = 486.33;
  postfit["WmunuB_v221"][3]    = 1252.10;

  prefit["WtaunuB_v221"][2]     = 305.00;
  prefit["WtaunuB_v221"][3]     = 783.20;
  postfit["WtaunuB_v221"][2]    = 486.33;
  postfit["WtaunuB_v221"][3]    = 1252.10;

  double sf = postfit[sample][nJ] / prefit[sample][nJ];
  std::cout << "Event of sample " << sample << " and nJ = " << nJ << " has SF " << sf << std::endl;

  return sf;
}

vector<TString> GetTokens(TString listOrig, TString sep)
{
   TObjArray *arrayList = listOrig.Tokenize(sep);

   return(arrayList2Vec(arrayList));
}


vector<TString> arrayList2Vec(TObjArray *arrayList)
{
   vector<TString> vect;
   for (Int_t i = 0; i < arrayList->GetEntries(); i++)
   {
      vect.push_back(((TObjString *)(arrayList->At(i)))->String());
   }

   return(vect);
}


int main(int argc, char **argv)
{
   TString dataset(argv[1]);
   TString sigList(argv[2]);
   TString bkgList(argv[3]);
   TString varList(argv[4]);

   TString varList3j = (argc > 5) ? TString(argv[5]) : "";
   TString comment   = (argc > 6) ? TString(argv[6]) : "";
   TString options   = (argc > 7) ? TString(argv[7]) : "";

   cout << "argc = " << argc << endl;

   cout << "dataset : " << dataset << endl;
   cout << "sigList : " << sigList << endl;
   cout << "bkgList : " << bkgList << endl;
   cout << "varList : " << varList << endl;
   cout << "varList3j : " << varList3j << endl;
   cout << "comment : " << comment << endl;

   TMVAClassification(dataset, sigList, bkgList, varList, varList3j, comment, options);

   return(0);
}
